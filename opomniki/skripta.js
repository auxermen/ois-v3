window.addEventListener("load", function() {
	// Stran naložena
		
	var izvediPrijavo = function() {
		var ime = document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	};
	document.querySelector("#prijavniGumb").addEventListener("click", izvediPrijavo);
	
	var dodajOpomnik = function() {
		var naziv = document.querySelector("#naziv_opomnika").value;
		var cas = document.querySelector("#cas_opomnika").value;
		document.querySelector("#naziv_opomnika").value = '';
		document.querySelector("#cas_opomnika").value = '';
		var opomniki = document.querySelector("#opomniki");
		opomniki.innerHTML += " \
		<div class='opomnik senca rob'> \
			<div class='naziv_opomnika'>" + naziv + "</div> \
			<div class='cas_opomnika'>Opomnik čez <span>" + cas +"</span> sekund.</div> \
		</div>";
	};
	document.querySelector("#dodajGumb").addEventListener("click", dodajOpomnik);
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// - če je čas enak 0, izpiši opozorilo
			if (cas == 0) {
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev '" + naziv_opomnika + "' je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			else {
				casovnik.innerHTML = cas - 1;
			}
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
});